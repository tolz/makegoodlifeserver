var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('vis/index', { title: 'vis' });
  
});

module.exports = router;
