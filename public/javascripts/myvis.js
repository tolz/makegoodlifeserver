// create an array with nodes
var nodes = new vis.DataSet([ {
	id : 1,
	label : '西野研究室'
}, {
	id : 2,
	label : 'ファジィ'
}, {
	id : 3,
	label : 'ミニ四駆AI'
}, {
	id : 4,
	label : 'プロジェクトオイラー'
}, {
	id : 5,
	label : 'TUBSTAP'
} ]);


var nodes_search_word = [
	"電通大 西野研究室",
	"電通大 ファジィ",
	"電通大 ミニ四駆AI",
	"電通大 プロジェクトオイラー",
	"電通大 TUBSTAP"
]

// create an array with edges
var edges = new vis.DataSet([ {
	from : 1,
	to : 3
}, {
	from : 1,
	to : 2
}, {
	from : 2,
	to : 4
}, {
	from : 2,
	to : 5
}, {
	from : 3,
	to : 3
} ]);

// create a network
var container = document.getElementById('mynetwork');
var data = {
	nodes : nodes,
	edges : edges
};
var options = {};
var network = new vis.Network(container, data, options);

function findSearchWordById(id) {
	for (var i in nodes_search_word) {
		if(i == id - 1)
			return nodes_search_word[i];
	}
	return null;
}

network.on("doubleClick", function (params) {
	console.log(params["nodes"][0]);
	window.open("https://www.google.co.jp/search?q=" + findSearchWordById(params["nodes"][0]), '_blank');
});

